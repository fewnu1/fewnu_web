import 'package:flutter/material.dart';

import '../../constants/app_layout.dart';

class FooterSection extends StatelessWidget {
  const FooterSection({super.key});

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return screenSize.width < 1155
        ? Container(
            //
            padding: EdgeInsets.only(top: AppLayout.getHeight(30)),
            height: AppLayout.getScreenHeight() * 1.1,
            width: AppLayout.getScreenWidth(),
            decoration: const BoxDecoration(
              color: Color(0xFFF6F7FF),
            ),
            child: Column(
              children: [
                //---footer
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    //--logo---
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Image(
                          image: AssetImage("assets/images/super_app.png"),
                        ),
                        SizedBox(height: AppLayout.getHeight(20)),
                        SizedBox(
                          width: AppLayout.getWidth(400),
                          child: const Text(
                              "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusm od tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."),
                        ),
                        SizedBox(height: AppLayout.getHeight(15)),
                        SizedBox(
                          width: 400,
                          child: Row(
                            children: [
                              SizedBox(
                                width: AppLayout.getWidth(190),
                                child: const Image(
                                  image: AssetImage("assets/images/Link_1.png"),
                                ),
                              ),
                              SizedBox(width: AppLayout.getWidth(20)),
                              SizedBox(
                                width: AppLayout.getWidth(190),
                                child: const Image(
                                  image: AssetImage("assets/images/Link_2.png"),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                //---
                //--Separateur
                SizedBox(height: AppLayout.getHeight(20)),
                SizedBox(
                  width: AppLayout.getWidth(800),
                  child: const Divider(
                    height: 2, // Épaisseur de 2 pixels
                    color: Color(0xFFE6E9FF), // Couleur bleue
                  ),
                ),
                SizedBox(height: AppLayout.getHeight(20)),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    //---Entreprise--
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Entreprise",
                          style: TextStyle(
                            fontSize: AppLayout.getHeight(25),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: AppLayout.getHeight(20)),
                        const Text(
                          "Accueil",
                        ),
                        SizedBox(height: AppLayout.getHeight(10)),
                        const Text(
                          "A propos de nous",
                        ),
                        SizedBox(height: AppLayout.getHeight(10)),
                        const Text(
                          "Nos services",
                        ),
                        SizedBox(height: AppLayout.getHeight(10)),
                        const Text(
                          "Captures d'écran",
                        ),
                        SizedBox(height: AppLayout.getHeight(10)),
                        const Text(
                          "Blog",
                        ),
                        SizedBox(height: AppLayout.getHeight(10)),
                        const Text(
                          "Témoignages",
                        ),
                        SizedBox(height: AppLayout.getHeight(10)),
                        const Text(
                          "Contact",
                        ),
                      ],
                    ),
                    //---Adresse
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Adresse",
                          style: TextStyle(
                            fontSize: AppLayout.getHeight(25),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: AppLayout.getHeight(20)),
                        const Text(
                          "HLM Grand yoof",
                        ),
                        SizedBox(height: AppLayout.getHeight(10)),
                        const Text(
                          "17.000, Dakar, Senegal",
                        ),
                        SizedBox(height: AppLayout.getHeight(10)),
                        Row(
                          children: [
                            const Text(
                              "Appelez-nous :",
                              style: TextStyle(
                                color: Color(0xFFACAECF),
                              ),
                            ),
                            SizedBox(width: AppLayout.getWidth(5.0)),
                            const Text(
                              "+221 77 777 77 77",
                              style: TextStyle(
                                color: Color(0xFF20CDD8),
                              ),
                            )
                          ],
                        ),
                        SizedBox(height: AppLayout.getHeight(10)),
                        Row(
                          children: [
                            const Text(
                              "Par courrier à :",
                              style: TextStyle(
                                color: Color(0xFFACAECF),
                              ),
                            ),
                            SizedBox(width: AppLayout.getWidth(5.0)),
                            const Text(
                              "info@gmail.com",
                              style: TextStyle(
                                color: Color(0xFF20CDD8),
                              ),
                            )
                          ],
                        ),
                        SizedBox(height: AppLayout.getHeight(10)),
                        Row(
                          children: [
                            const Text(
                              "Site web :",
                              style: TextStyle(
                                color: Color(0xFFACAECF),
                              ),
                            ),
                            SizedBox(width: AppLayout.getWidth(5.0)),
                            const Text(
                              "www.fewnu.app",
                              style: TextStyle(
                                color: Color(0xFF20CDD8),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                    //---
                  ],
                ),

                //---Copyright
                SizedBox(height: AppLayout.getHeight(20)),
                Container(
                  margin: EdgeInsets.only(
                    left: AppLayout.getWidth(40),
                    right: AppLayout.getWidth(20),
                  ),
                  child: Row(
                    //crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      //--Text---
                      const Row(
                        children: [
                          Text("Copyright By  "),
                          Text(
                            "BasicTheme  ",
                            style: TextStyle(
                              color: Color(0xFF20CDD8),
                            ),
                          ),
                          Text("2019"),
                        ],
                      ),
                      //---médias sociaux
                      Row(
                        children: [
                          InkWell(
                            onTap: () {
                              // Action à effectuer lorsque l'icône de média social est cliquée
                            },
                            child: Container(
                              width: 32.0,
                              height: 32.0,
                              decoration: const BoxDecoration(
                                color: Colors.grey,
                                shape: BoxShape.circle,
                              ),
                              child: const Center(
                                child: Icon(
                                  Icons.facebook,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(width: AppLayout.getHeight(10)),
                          InkWell(
                            onTap: () {
                              // Action à effectuer lorsque l'icône de média social est cliquée
                            },
                            child: Container(
                              width: 32.0,
                              height: 32.0,
                              decoration: const BoxDecoration(
                                color: Colors.grey,
                                shape: BoxShape.circle,
                              ),
                              child: const Center(
                                child: Icon(
                                  Icons.facebook,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        //---Large Screen---
        : Container(
            padding: EdgeInsets.only(
              top: AppLayout.getHeight(30),
            ),
            height: AppLayout.getScreenHeight() * 0.70,
            width: AppLayout.getScreenWidth(),
            decoration: const BoxDecoration(
              color: Color(0xFFF6F7FF),
            ),
            child: Column(
              children: [
                //---footer
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    //--logo---
                    Container(
                      padding: EdgeInsets.only(left: AppLayout.getWidth(100)),
                      width: screenSize.width / 2,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Image(
                            image: AssetImage("assets/images/super_app.png"),
                          ),
                          SizedBox(height: AppLayout.getHeight(20)),
                          SizedBox(
                            width: AppLayout.getWidth(400),
                            child: const Text(
                                "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusm od tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."),
                          ),
                          SizedBox(height: AppLayout.getHeight(15)),
                          SizedBox(
                            width: 400,
                            child: Row(
                              children: [
                                SizedBox(
                                  width: AppLayout.getWidth(190),
                                  child: const Image(
                                    image:
                                        AssetImage("assets/images/Link_1.png"),
                                  ),
                                ),
                                SizedBox(width: AppLayout.getWidth(20)),
                                SizedBox(
                                  width: AppLayout.getWidth(190),
                                  child: const Image(
                                    image:
                                        AssetImage("assets/images/Link_2.png"),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    //---Entreprise--
                    SizedBox(
                      width: screenSize.width / 4,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Entreprise",
                            style: TextStyle(
                              fontSize: AppLayout.getHeight(25),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(height: AppLayout.getHeight(20)),
                          const Text(
                            "Accueil",
                          ),
                          SizedBox(height: AppLayout.getHeight(10)),
                          const Text(
                            "A propos de nous",
                          ),
                          SizedBox(height: AppLayout.getHeight(10)),
                          const Text(
                            "Nos services",
                          ),
                          SizedBox(height: AppLayout.getHeight(10)),
                          const Text(
                            "Captures d'écran",
                          ),
                          SizedBox(height: AppLayout.getHeight(10)),
                          const Text(
                            "Blog",
                          ),
                          SizedBox(height: AppLayout.getHeight(10)),
                          const Text(
                            "Témoignages",
                          ),
                          SizedBox(height: AppLayout.getHeight(10)),
                          const Text(
                            "Contact",
                          ),
                        ],
                      ),
                    ),
                    //---Adresse
                    SizedBox(
                      width: screenSize.width / 4,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Adresse",
                            style: TextStyle(
                              fontSize: AppLayout.getHeight(25),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(height: AppLayout.getHeight(20)),
                          const Text(
                            "HLM Grand yoof",
                          ),
                          SizedBox(height: AppLayout.getHeight(10)),
                          const Text(
                            "17.000, Dakar, Senegal",
                          ),
                          SizedBox(height: AppLayout.getHeight(10)),
                          Row(
                            children: [
                              const Text(
                                "Appelez-nous :",
                              ),
                              SizedBox(width: AppLayout.getWidth(5.0)),
                              const Text(
                                "+221 77 777 77 77",
                                style: TextStyle(
                                  color: Color(0xFF20CDD8),
                                ),
                              )
                            ],
                          ),
                          SizedBox(height: AppLayout.getHeight(10)),
                          Row(
                            children: [
                              const Text(
                                "Par courrier à :",
                              ),
                              SizedBox(width: AppLayout.getWidth(5.0)),
                              const Text(
                                "info@gmail.com",
                                style: TextStyle(
                                  color: Color(0xFF20CDD8),
                                ),
                              )
                            ],
                          ),
                          SizedBox(height: AppLayout.getHeight(10)),
                          Row(
                            children: [
                              const Text(
                                "Site web :",
                              ),
                              SizedBox(width: AppLayout.getWidth(5.0)),
                              const Text(
                                "www.fewnu.app",
                                style: TextStyle(
                                  color: Color(0xFF20CDD8),
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                    //---
                  ],
                ),
                //--Separateur
                SizedBox(height: AppLayout.getHeight(20)),
                SizedBox(
                  width: AppLayout.getWidth(800),
                  child: const Divider(
                    height: 2, // Épaisseur de 2 pixels
                    color: Color(0xFFE6E9FF), // Couleur bleue
                  ),
                ),
                //---Copyright
                SizedBox(height: AppLayout.getHeight(40)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    //--Text---
                    Container(
                      padding: EdgeInsets.only(left: AppLayout.getWidth(100)),
                      width: screenSize.width * 0.75,
                      child: const Row(
                        children: [
                          Text("Copyright By  "),
                          Text(
                            "BasicTheme  ",
                            style: TextStyle(
                              color: Color(0xFF20CDD8),
                            ),
                          ),
                          Text("2019"),
                        ],
                      ),
                    ),
                    //---médias sociaux
                    SizedBox(
                      width: screenSize.width / 4,
                      child: Row(
                        children: [
                          InkWell(
                            onTap: () {
                              // Action à effectuer lorsque l'icône de média social est cliquée
                            },
                            child: Container(
                              width: 32.0,
                              height: 32.0,
                              decoration: const BoxDecoration(
                                color: Colors.grey,
                                shape: BoxShape.circle,
                              ),
                              child: const Center(
                                child: Icon(
                                  Icons.facebook,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(width: AppLayout.getHeight(10)),
                          InkWell(
                            onTap: () {
                              // Action à effectuer lorsque l'icône de média social est cliquée
                            },
                            child: Container(
                              width: 32.0,
                              height: 32.0,
                              decoration: const BoxDecoration(
                                color: Colors.grey,
                                shape: BoxShape.circle,
                              ),
                              child: const Center(
                                child: Icon(
                                  Icons.facebook,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          );
  }
}
