import 'package:flutter/material.dart';

import '../../constants/app_layout.dart';

class ActuWidget extends StatelessWidget {
  final String iconAssetPath; // Chemin de l'image dans les assets
  final String title;
  final String description;

  const ActuWidget({
    super.key,
    required this.iconAssetPath,
    required this.title,
    required this.description,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      //padding: const EdgeInsets.all(16),
      decoration: const BoxDecoration(
        color: Colors.white,
      ),
      width: AppLayout.getWidth(300),
      child: Column(
        children: [
          SizedBox(
            width: AppLayout.getWidth(300),
            height: AppLayout.getHeight(175),
            child: Image.asset(
              iconAssetPath,
              height: AppLayout.getHeight(40), // Hauteur de l'image
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(height: AppLayout.getHeight(15)),
          Padding(
            padding: EdgeInsets.only(
              left: AppLayout.getWidth(20),
              right: AppLayout.getWidth(20),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: TextStyle(
                    fontSize: AppLayout.getHeight(14),
                    color: const Color(0xFF029CAD),
                  ),
                ),
                SizedBox(height: AppLayout.getHeight(10)),
                Text(
                  description,
                  style: TextStyle(
                    fontSize: AppLayout.getHeight(18),
                    fontWeight: FontWeight.bold,
                  ),
                ),
                //---
                SizedBox(height: AppLayout.getHeight(10)),
                const Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    //---j'aime----
                    Row(
                      children: [
                        //
                        Icon(
                          Icons.favorite_border_outlined,
                          color: Color(0xFF787878),
                        ),
                        Text(
                          "45",
                          style: TextStyle(
                            color: Color(0xFF787878),
                          ),
                        ),
                        Text(
                          "J'aime",
                          style: TextStyle(
                            color: Color(0xFF787878),
                          ),
                        ),
                      ],
                    ),
                    //---commentaires---
                    Row(
                      children: [
                        //
                        Icon(
                          Icons.message_outlined,
                          color: Color(0xFF787878),
                        ),
                        Text(
                          "14",
                          style: TextStyle(
                            color: Color(0xFF787878),
                          ),
                        ),
                        Text(
                          "Commentaires",
                          style: TextStyle(
                            color: Color(0xFF787878),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: AppLayout.getHeight(10)),
        ],
      ),
    );
  }
}
