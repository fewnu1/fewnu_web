import 'package:flutter/material.dart';

import '../../constants/app_layout.dart';
import 'investir_section.dart';

class AproposSection extends StatelessWidget {
  const AproposSection({super.key});

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return screenSize.width < 1155
        ? SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: AppLayout.getScreenHeight() * 0.99,
                  child: Column(
                    children: [
                      SizedBox(height: AppLayout.getHeight(35)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Qui sommes nous ?",
                            style: TextStyle(
                              fontSize: AppLayout.getHeight(25),
                              color: const Color(0xFF029CAD),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: AppLayout.getHeight(20)),
                      SizedBox(
                        height: AppLayout.getScreenHeight() * 0.40,
                        child: const Image(
                          image: AssetImage("assets/images/img_1.png"),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            width: AppLayout.getWidth(50),
                            child: const Image(
                              image: AssetImage("assets/images/img_6.png"),
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                "Nous sommes Fewnu Super App, \nl'application qui simplifie votre vie.",
                                style: TextStyle(
                                  fontSize: AppLayout.getHeight(25),
                                ),
                              ),
                              SizedBox(height: AppLayout.getHeight(20)),
                              Text(
                                'Adipisicing elit, sed do eiusmod tempor incididunt with labore \net dolore magna aliqua enim ad minimum.',
                                style: TextStyle(
                                  fontSize: AppLayout.getHeight(14),
                                ),
                              ),
                              Text(
                                'Adipisicing elit, sed do eiusmod tempor incididunt with labore \net dolore magna aliqua enim ad minimum.',
                                style: TextStyle(
                                  fontSize: AppLayout.getHeight(14),
                                  //color: Colors.white,
                                ),
                              ),
                              SizedBox(height: AppLayout.getHeight(20)),
                              ElevatedButton(
                                onPressed: () {
                                  // Action à effectuer lorsque le bouton est cliqué
                                },
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: const Color(
                                      0xFF029CAD), // Couleur de fond du bouton
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(
                                        0), // Bord arrondi à zéro
                                  ),
                                ),
                                child: Text(
                                  "Apprendre encore plus",
                                  style: TextStyle(
                                    color: Colors.white, // Couleur du texte
                                    fontSize: AppLayout.getHeight(
                                        16), // Taille du texte
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                //---investissement--
                const InvestirSection(),
                //---footer--
                //const FooterSection()
              ],
            ),
          )
        //----large screen
        : SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: AppLayout.getHeight(35)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      width: AppLayout.getWidth(310),
                    ),
                    Text(
                      "Qui sommes nous ?",
                      style: TextStyle(
                        fontSize: AppLayout.getHeight(25),
                        color: const Color(0xFF029CAD),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: AppLayout.getHeight(20)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: AppLayout.getHeight(500),
                      child: const Image(
                        image: AssetImage("assets/images/img_6.png"),
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: AppLayout.getHeight(40)),
                        Text(
                          "Nous sommes Fewnu Super App, \nl'application qui simplifie votre vie.",
                          style: TextStyle(
                            fontSize: AppLayout.getHeight(25),
                          ),
                        ),
                        SizedBox(height: AppLayout.getHeight(40)),
                        Text(
                          'Adipisicing elit, sed do eiusmod tempor incididunt with labore \net dolore magna aliqua enim ad minimum.',
                          style: TextStyle(
                            fontSize: AppLayout.getHeight(14),
                          ),
                        ),
                        Text(
                          'Adipisicing elit, sed do eiusmod tempor incididunt with labore \net dolore magna aliqua enim ad minimum.',
                          style: TextStyle(
                            fontSize: AppLayout.getHeight(14),
                            //color: Colors.white,
                          ),
                        ),
                        SizedBox(height: AppLayout.getHeight(50)),
                        ElevatedButton(
                          onPressed: () {
                            // Action à effectuer lorsque le bouton est cliqué
                          },
                          style: ElevatedButton.styleFrom(
                            backgroundColor: const Color(
                                0xFF029CAD), // Couleur de fond du bouton
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(
                                  0), // Bord arrondi à zéro
                            ),
                          ),
                          child: Text(
                            "Apprendre encore plus",
                            style: TextStyle(
                              color: Colors.white, // Couleur du texte
                              fontSize:
                                  AppLayout.getHeight(16), // Taille du texte
                            ),
                          ),
                        ),
                      ],
                    ),
                    Container(
                      padding: EdgeInsets.only(
                        bottom: AppLayout.getHeight(100),
                      ),
                      height: AppLayout.getScreenHeight() * 0.85,
                      //width: AppLayout.getWidth(900),
                      child: const Image(
                        image: AssetImage("assets/images/img_1.png"),
                      ),
                    ),
                  ],
                ),
                //---investissement--
                const InvestirSection(),
                //---footer--
                //const FooterSection()
              ],
            ),
          );
  }
}
