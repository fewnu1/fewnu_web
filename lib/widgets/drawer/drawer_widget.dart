// ignore_for_file: library_private_types_in_public_api

import 'package:flutter/material.dart';

import '../../constants/app_layout.dart';
import '../../constants/colors.dart';

class DrawerWidget extends StatefulWidget {
  const DrawerWidget({Key? key}) : super(key: key);

  @override
  _DrawerWidgetState createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends State<DrawerWidget> {
  final ScrollController _scrollController = ScrollController();

  void _navigateToSection(double position) {
    Navigator.pop(context); // Ferme le Drawer
    _scrollController.animateTo(
      position,
      duration: const Duration(milliseconds: 500),
      curve: Curves.easeInOut,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          Container(
            padding: EdgeInsets.only(
              left: AppLayout.getWidth(20),
              top: AppLayout.getWidth(20),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                TextButton(
                  onPressed: () => _navigateToSection(0),
                  child: const Text(
                    "Accueil",
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                TextButton(
                  onPressed: () => _navigateToSection(
                    MediaQuery.of(context).size.height * 0.85,
                  ),
                  child: const Text(
                    "A propos",
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                // Répétez pour les autres sections
                //--debut bouton--
                SizedBox(width: AppLayout.getWidth(60)),
                MouseRegion(
                  cursor: SystemMouseCursors.click,
                  child: GestureDetector(
                    onTap: () {
                      // Gérer le clic sur le bouton
                    },
                    child: Container(
                      width: 200,
                      height: 40,
                      decoration: BoxDecoration(
                        color: AppColors.mainColor,
                        borderRadius: BorderRadius.circular(0),
                      ),
                      child: const Center(
                        child: Text(
                          "Commencer Maintenant",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                //--fin bouton---
              ],
            ),
          ),
        ],
      ),
    );
  }
}
