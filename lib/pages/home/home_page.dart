import 'package:flutter/material.dart';

import '../../constants/app_layout.dart';
import '../../constants/colors.dart';
//import '../../widgets/drawer/drawer_widget.dart';
import '../sections/apropos_section.dart';
import '../sections/blog_section.dart';
import '../sections/capture_section.dart';
import '../sections/contact_section.dart';
import '../sections/home_section.dart';
import '../sections/service_section.dart';
import '../sections/temoignage_section.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  //
  final ScrollController _scrollController = ScrollController();

  void _navigateToSection(double position) {
    Navigator.pop(context); // Ferme le Drawer
    _scrollController.animateTo(
      position,
      duration: const Duration(milliseconds: 500),
      curve: Curves.easeInOut,
    );
  }

  //
  @override
  Widget build(BuildContext context) {
    //
    final screenSize = MediaQuery.of(context).size;
    //
    return Scaffold(
      appBar: screenSize.width <= 1200
          ? AppBar(
              //--ici--
              backgroundColor: Colors.white,
              iconTheme: const IconThemeData(color: Color(0xFF01ABB9)),
              title: const Center(
                child: Image(
                  image: AssetImage("assets/images/super_app.png"),
                ),
              ),
            )
          : AppBar(
              backgroundColor: Colors.white,
              //automaticallyImplyLeading: false, // Pour enlever le bouton de retour
              actions: [
                Container(
                  padding: EdgeInsets.only(left: AppLayout.getWidth(100)),
                  width: screenSize.width,
                  child: Row(
                    children: [
                      Image.asset("assets/images/super_app.png"),
                      SizedBox(width: AppLayout.getWidth(60)),
                      TextButton(
                        onPressed: () {
                          _scrollController.animateTo(
                            0,
                            duration: const Duration(milliseconds: 500),
                            curve: Curves.easeInOut,
                          );
                        },
                        child: const Text(
                          "Accueil",
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      TextButton(
                        onPressed: () {
                          _scrollController.animateTo(
                            MediaQuery.of(context).size.height * 0.85,
                            duration: const Duration(milliseconds: 500),
                            curve: Curves.easeInOut,
                          );
                        },
                        child: const Text(
                          "A propos",
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      TextButton(
                        onPressed: () {
                          _scrollController.animateTo(
                            MediaQuery.of(context).size.height * 2.85,
                            duration: const Duration(milliseconds: 500),
                            curve: Curves.easeInOut,
                          );
                        },
                        child: const Text(
                          "Nos services",
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      TextButton(
                        onPressed: () {
                          _scrollController.animateTo(
                            MediaQuery.of(context).size.height * 3.65,
                            duration: const Duration(milliseconds: 500),
                            curve: Curves.easeInOut,
                          );
                        },
                        child: const Text(
                          "Capture d'écran",
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      TextButton(
                        onPressed: () {
                          _scrollController.animateTo(
                            MediaQuery.of(context).size.height * 4.55,
                            duration: const Duration(milliseconds: 500),
                            curve: Curves.easeInOut,
                          );
                        },
                        child: const Text(
                          "Blog",
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      TextButton(
                        onPressed: () {
                          _scrollController.animateTo(
                            MediaQuery.of(context).size.height * 5.52,
                            duration: const Duration(milliseconds: 500),
                            curve: Curves.easeInOut,
                          );
                        },
                        child: const Text(
                          "Témoignage",
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      TextButton(
                        onPressed: () {
                          _scrollController.animateTo(
                            MediaQuery.of(context).size.height * 6.36,
                            duration: const Duration(milliseconds: 500),
                            curve: Curves.easeInOut,
                          );
                        },
                        child: const Text(
                          "Contact",
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      //--debut bouton--
                      SizedBox(width: AppLayout.getWidth(60)),
                      MouseRegion(
                        cursor: SystemMouseCursors.click,
                        child: GestureDetector(
                          onTap: () {
                            //--clic sur le bouton--
                          },
                          child: Container(
                            width: 200,
                            height: 40,
                            decoration: BoxDecoration(
                              color: AppColors.mainColor,
                              borderRadius: BorderRadius.circular(0),
                            ),
                            child: const Center(
                              child: Text(
                                "Commencer Maintenant",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                        ),
                      )
                      //--fin bouton---
                    ],
                  ),
                ),
              ],
            ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            Container(
              padding: EdgeInsets.only(
                left: AppLayout.getWidth(20),
                top: AppLayout.getWidth(20),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  SizedBox(height: AppLayout.getHeight(20)),
                  TextButton(
                    onPressed: () => _navigateToSection(0),
                    child: const Text(
                      "Accueil",
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  TextButton(
                    onPressed: () => _navigateToSection(
                      MediaQuery.of(context).size.height * 0.85,
                    ),
                    child: const Text(
                      "A propos",
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  TextButton(
                    onPressed: () => _navigateToSection(
                      MediaQuery.of(context).size.height * 2.72,
                    ),
                    child: const Text(
                      "Nos services",
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  TextButton(
                    onPressed: () => _navigateToSection(
                      MediaQuery.of(context).size.height * 4.10,
                    ),
                    child: const Text(
                      "Capture d'écran",
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  TextButton(
                    onPressed: () => _navigateToSection(
                      MediaQuery.of(context).size.height * 5.55,
                    ),
                    child: const Text(
                      "Blog  ",
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  TextButton(
                    onPressed: () => _navigateToSection(
                      MediaQuery.of(context).size.height * 7.60,
                    ),
                    child: const Text(
                      "Témoignage",
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  TextButton(
                    onPressed: () => _navigateToSection(
                      MediaQuery.of(context).size.height * 8.45,
                    ),
                    child: const Text(
                      "Contact",
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  //--debut bouton--
                  SizedBox(width: AppLayout.getWidth(60)),
                  MouseRegion(
                    cursor: SystemMouseCursors.click,
                    child: GestureDetector(
                      onTap: () {
                        // Gérer le clic sur le bouton
                      },
                      child: Container(
                        width: 200,
                        height: 40,
                        decoration: BoxDecoration(
                          color: AppColors.mainColor,
                          borderRadius: BorderRadius.circular(0),
                        ),
                        child: const Center(
                          child: Text(
                            "Commencer Maintenant",
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  //--fin bouton---
                ],
              ),
            ),
          ],
        ),
      ),
      body: ListView(
        controller: _scrollController,
        children: const [
          SectionPage(
            section: HomeSection(),
          ),
          SectionPage(
            section: AproposSection(),
          ),
          SectionPage(
            section: ServiceSection(),
          ),
          SectionPage(
            section: CaptureSection(),
          ),
          SectionPage(
            section: BlogSection(),
          ),
          SectionPage(
            section: TemoignageSection(),
          ),
          SectionPage(
            section: ContactSection(),
          ),
        ],
      ),
    );
  }
}

class SectionPage extends StatelessWidget {
  final Widget section;

  const SectionPage({
    super.key,
    required this.section,
  });

  @override
  Widget build(BuildContext context) {
    return section;
  }
}
