import 'package:flutter/material.dart';

import '../../constants/app_layout.dart';
import '../../widgets/contact/contact_widget.dart';
import 'footer_section.dart';

class ContactSection extends StatelessWidget {
  const ContactSection({super.key});

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(top: AppLayout.getHeight(30)),
            height: AppLayout.getScreenHeight() * 1.1,
            width: screenSize.width,
            decoration: const BoxDecoration(
              color: Color(0xFF000000),
            ),
            child: Column(
              // crossAxisAlignment: CrossAxisAlignment.center,
              // mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Contact",
                  style: TextStyle(
                    fontSize: AppLayout.getHeight(18),
                    color: const Color(0xFFffffff),
                  ),
                ),
                SizedBox(height: AppLayout.getHeight(20)),
                Text(
                  "Entrer en contact",
                  style: TextStyle(
                    fontSize: AppLayout.getHeight(25),
                    color: const Color(0xFFffffff),
                  ),
                ),
                //------les commentaires-----
                SizedBox(height: AppLayout.getHeight(40)),
                //--ici---
                const ContactWidget()
              ],
            ),
          ),
          //---footer--
          const FooterSection()
        ],
      ),
    );
  }
}
