import 'package:flutter/material.dart';

import '../../constants/app_layout.dart';
import '../../widgets/actus/actu_widget.dart';

class BlogSection extends StatelessWidget {
  const BlogSection({super.key});

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return screenSize.width < 1155
        ? SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  //---
                  padding: EdgeInsets.only(top: AppLayout.getHeight(50)),
                  height: AppLayout.getScreenHeight() * 2.1,
                  width: AppLayout.getScreenWidth(),
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                          'assets/images/img_5.png'), // Remplacez par le chemin de votre image
                      fit: BoxFit.cover, // Pour remplir tout le container
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "Fil d'actualité",
                        style: TextStyle(
                          fontSize: AppLayout.getHeight(18),
                          color: Colors.white,
                        ),
                      ),
                      Text(
                        'Obtenez chaque mise à jour',
                        style: TextStyle(
                          fontSize: AppLayout.getHeight(25),
                          color: Colors.white,
                        ),
                      ),
                      Text(
                        "sur notre fil d'actualité",
                        style: TextStyle(
                          fontSize: AppLayout.getHeight(20),
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(height: AppLayout.getHeight(10)),
                      //-----ICI-----
                      Padding(
                        padding: EdgeInsets.only(
                          left: AppLayout.getWidth(20),
                          right: AppLayout.getWidth(20),
                        ),
                        child: Column(
                          children: [
                            const ActuWidget(
                              iconAssetPath: "assets/images/actu_1.png",
                              title: "Toupet Logiciel",
                              description:
                                  "Lorem ipsum dolor sit amet consectetur. In.",
                            ),
                            SizedBox(height: AppLayout.getHeight(10)),
                            const ActuWidget(
                              iconAssetPath: "assets/images/actu_2.png",
                              title: "Toupet Logiciel",
                              description:
                                  "Lorem ipsum dolor sit amet consectetur. In.",
                            ),
                            //
                            SizedBox(height: AppLayout.getHeight(10)),
                            const ActuWidget(
                              iconAssetPath: "assets/images/actu_2.png",
                              title: "Toupet Logiciel",
                              description:
                                  "Lorem ipsum dolor sit amet consectetur. In.",
                            ),
                            //
                          ],
                        ),
                      )
                      //
                    ],
                  ),
                ),
                //---footer--
                // const FooterSection()
              ],
            ),
          )
        //---large screen---
        : SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.only(top: AppLayout.getHeight(50)),
                  height: AppLayout.getScreenHeight() * 0.99,
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                          'assets/images/img_5.png'), // Remplacez par le chemin de votre image
                      fit: BoxFit.cover, // Pour remplir tout le container
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "Fil d'actualité",
                        style: TextStyle(
                          fontSize: AppLayout.getHeight(18),
                          color: Colors.white,
                        ),
                      ),
                      Text(
                        'Obtenez chaque mise à jour',
                        style: TextStyle(
                          fontSize: AppLayout.getHeight(35),
                          color: Colors.white,
                        ),
                      ),
                      Text(
                        "sur notre fil d'actualité",
                        style: TextStyle(
                          fontSize: AppLayout.getHeight(35),
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(height: AppLayout.getHeight(10)),
                      //-----ICI-----
                      Padding(
                        padding: EdgeInsets.only(
                          left: AppLayout.getWidth(20),
                          right: AppLayout.getWidth(20),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            //
                            const ActuWidget(
                              iconAssetPath: "assets/images/actu_1.png",
                              title: "Toupet Logiciel",
                              description:
                                  "Lorem ipsum dolor sit amet consectetur. In.",
                            ),
                            SizedBox(width: AppLayout.getWidth(10)),
                            const ActuWidget(
                              iconAssetPath: "assets/images/actu_2.png",
                              title: "Toupet Logiciel",
                              description:
                                  "Lorem ipsum dolor sit amet consectetur. In.",
                            ),
                            SizedBox(width: AppLayout.getWidth(10)),
                            const ActuWidget(
                              iconAssetPath: "assets/images/actu_2.png",
                              title: "Toupet Logiciel",
                              description:
                                  "Lorem ipsum dolor sit amet consectetur. In.",
                            ),
                          ],
                        ),
                      )
                      //
                    ],
                  ),
                ),
                //---footer--
                // const FooterSection()
              ],
            ),
          );
  }
}
