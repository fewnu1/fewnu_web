import 'package:flutter/material.dart';

import '../../constants/app_layout.dart';

class CaptureSection extends StatelessWidget {
  const CaptureSection({super.key});

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return screenSize.width < 1155
        ? SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  //----
                  padding: EdgeInsets.only(top: AppLayout.getHeight(30)),
                  height: AppLayout.getScreenHeight() * 1.4,
                  width: AppLayout.getScreenWidth(),
                  decoration: const BoxDecoration(
                    color: Color(0xFFF6F7FF),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "Écrans",
                        style: TextStyle(
                          fontSize: AppLayout.getHeight(18),
                          color: const Color(0xFF029CAD),
                        ),
                      ),
                      SizedBox(height: AppLayout.getHeight(20)),
                      Text(
                        "Capture d'écran de l'application.",
                        style: TextStyle(
                          fontSize: AppLayout.getHeight(25),
                        ),
                      ),
                      //------les capture-----
                      SizedBox(height: AppLayout.getHeight(20)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          //
                          SizedBox(
                            height: AppLayout.getScreenHeight() * 0.55,
                            child: const Image(
                              image: AssetImage("assets/images/cap_1.png"),
                            ),
                          ),
                          SizedBox(width: AppLayout.getWidth(10)),
                          SizedBox(
                            height: AppLayout.getScreenHeight() * 0.55,
                            child: const Image(
                              image: AssetImage("assets/images/cap_2.png"),
                            ),
                          ),
                          SizedBox(width: AppLayout.getWidth(10)),
                          SizedBox(
                            height: AppLayout.getScreenHeight() * 0.55,
                            child: const Image(
                              image: AssetImage("assets/images/cap_3.png"),
                            ),
                          ),
                        ],
                      ),
                      //---
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          //
                          SizedBox(width: AppLayout.getWidth(10)),
                          SizedBox(
                            height: AppLayout.getScreenHeight() * 0.55,
                            child: const Image(
                              image: AssetImage("assets/images/cap_3.png"),
                            ),
                          ),
                          SizedBox(width: AppLayout.getWidth(10)),
                          SizedBox(
                            height: AppLayout.getScreenHeight() * 0.55,
                            child: const Image(
                              image: AssetImage("assets/images/cap_4.png"),
                            ),
                          ),
                          SizedBox(width: AppLayout.getWidth(10)),
                          SizedBox(
                            height: AppLayout.getScreenHeight() * 0.55,
                            child: const Image(
                              image: AssetImage("assets/images/cap_5.png"),
                            ),
                          ),
                        ],
                      ),
                      //---
                    ],
                  ),
                ),
                //---footer--
                //const FooterSection()
              ],
            ),
          )
        //---large screen---
        : SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.only(top: AppLayout.getHeight(30)),
                  height: AppLayout.getScreenHeight() * 0.85,
                  width: AppLayout.getScreenWidth(),
                  decoration: const BoxDecoration(
                    color: Color(0xFFF6F7FF),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "Écrans",
                        style: TextStyle(
                          fontSize: AppLayout.getHeight(18),
                          color: const Color(0xFF029CAD),
                        ),
                      ),
                      SizedBox(height: AppLayout.getHeight(20)),
                      Text(
                        "Capture d'écran de l'application.",
                        style: TextStyle(
                          fontSize: AppLayout.getHeight(25),
                        ),
                      ),
                      //------les capture-----
                      SizedBox(height: AppLayout.getHeight(20)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          //
                          SizedBox(
                            height: AppLayout.getScreenHeight() * 0.55,
                            child: const Image(
                              image: AssetImage("assets/images/cap_1.png"),
                            ),
                          ),
                          SizedBox(width: AppLayout.getWidth(10)),
                          SizedBox(
                            height: AppLayout.getScreenHeight() * 0.55,
                            child: const Image(
                              image: AssetImage("assets/images/cap_2.png"),
                            ),
                          ),
                          SizedBox(width: AppLayout.getWidth(10)),
                          SizedBox(
                            height: AppLayout.getScreenHeight() * 0.55,
                            child: const Image(
                              image: AssetImage("assets/images/cap_3.png"),
                            ),
                          ),
                          SizedBox(width: AppLayout.getWidth(10)),
                          SizedBox(
                            height: AppLayout.getScreenHeight() * 0.55,
                            child: const Image(
                              image: AssetImage("assets/images/cap_4.png"),
                            ),
                          ),
                          SizedBox(width: AppLayout.getWidth(10)),
                          SizedBox(
                            height: AppLayout.getScreenHeight() * 0.55,
                            child: const Image(
                              image: AssetImage("assets/images/cap_5.png"),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                //---footer--
                //const FooterSection()
              ],
            ),
          );
  }
}
