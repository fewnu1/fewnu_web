import 'package:fewnu_landing_page/pages/home/home_page.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Fewnu Landing Page',
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}
