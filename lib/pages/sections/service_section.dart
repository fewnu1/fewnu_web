import 'package:flutter/material.dart';

import '../../constants/app_layout.dart';
import '../../widgets/services/service_widget.dart';

class ServiceSection extends StatelessWidget {
  const ServiceSection({super.key});

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return screenSize.width < 1155
        ? SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.only(top: AppLayout.getHeight(30)),
                  height: AppLayout.getScreenHeight() * 1.4,
                  width: AppLayout.getScreenWidth(),
                  decoration: const BoxDecoration(
                    color: Color(0xFFF6F7FF),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "Nos SERVICES",
                        style: TextStyle(
                          fontSize: AppLayout.getHeight(25),
                          color: const Color(0xFF029CAD),
                        ),
                      ),
                      SizedBox(height: AppLayout.getHeight(20)),
                      Text(
                        "Des services essentiels aux opportunités de revenus.",
                        style: TextStyle(
                          fontSize: AppLayout.getHeight(20),
                        ),
                      ),
                      Text(
                        "Nous sommes une plateforme tout-en-un.",
                        style: TextStyle(
                          fontSize: AppLayout.getHeight(20),
                        ),
                      ),
                      //------les services-----
                      SizedBox(height: AppLayout.getHeight(40)),
                      const Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          //
                          ServiceWidget(
                            iconAssetPath: "assets/icons/consom.png",
                            title: "Consomation",
                            description:
                                "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore.",
                          ),
                          ServiceWidget(
                            iconAssetPath: "assets/icons/conduc.png",
                            title: "Consomation",
                            description:
                                "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore.",
                          ),
                        ],
                      ),
                      //----
                      SizedBox(height: AppLayout.getHeight(20)),
                      //----
                      const Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          //
                          ServiceWidget(
                            iconAssetPath: "assets/icons/marchand.png",
                            title: "Consomation",
                            description:
                                "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore.",
                          ),
                          ServiceWidget(
                            iconAssetPath: "assets/icons/entreprise.png",
                            title: "Consomation",
                            description:
                                "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore.",
                          ),
                        ],
                      ),
                      //---
                    ],
                  ),
                ),
                //--footer---
                //const FooterSection()
              ],
            ),
          )
        //---large screen----
        : SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.only(top: AppLayout.getHeight(30)),
                  height: AppLayout.getScreenHeight() * 0.85,
                  width: AppLayout.getScreenWidth(),
                  decoration: const BoxDecoration(
                    color: Color(0xFFF6F7FF),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "Nos SERVICES",
                        style: TextStyle(
                          fontSize: AppLayout.getHeight(25),
                          color: const Color(0xFF029CAD),
                        ),
                      ),
                      SizedBox(height: AppLayout.getHeight(20)),
                      Text(
                        "Des services essentiels aux opportunités de revenus.",
                        style: TextStyle(
                          fontSize: AppLayout.getHeight(25),
                        ),
                      ),
                      Text(
                        "Nous sommes une plateforme tout-en-un.",
                        style: TextStyle(
                          fontSize: AppLayout.getHeight(25),
                        ),
                      ),
                      //------les services-----
                      SizedBox(height: AppLayout.getHeight(40)),
                      const Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          //
                          ServiceWidget(
                            iconAssetPath: "assets/icons/consom.png",
                            title: "Consomation",
                            description:
                                "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore.",
                          ),
                          ServiceWidget(
                            iconAssetPath: "assets/icons/conduc.png",
                            title: "Consomation",
                            description:
                                "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore.",
                          ),
                          ServiceWidget(
                            iconAssetPath: "assets/icons/marchand.png",
                            title: "Consomation",
                            description:
                                "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore.",
                          ),
                          ServiceWidget(
                            iconAssetPath: "assets/icons/entreprise.png",
                            title: "Consomation",
                            description:
                                "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore.",
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                //--footer--
                //const FooterSection()
              ],
            ),
          );
  }
}
