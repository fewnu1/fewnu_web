import 'package:flutter/material.dart';

import '../../constants/app_layout.dart';

class HomeSection extends StatelessWidget {
  const HomeSection({super.key});

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return screenSize.width < 1000
        ? SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  height: AppLayout.getScreenHeight() * 0.85,
                  width: AppLayout.getScreenWidth(),
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                          'assets/images/img_3.png'), // Remplacez par le chemin de votre image
                      fit: BoxFit.cover, // Pour remplir tout le container
                    ),
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                        height: AppLayout.getScreenHeight() * 0.40,
                        child: const Image(
                          image: AssetImage("assets/images/acceuil.png"),
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'Unifier vos applications \nen une seule expérience.',
                            style: TextStyle(
                              fontSize: AppLayout.getHeight(25),
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(height: AppLayout.getHeight(10)),
                          Text(
                            'Adipisicing elit, sed do eiusmod tempor incididunt with labore \net dolore magna aliqua enim ad minimum.',
                            style: TextStyle(
                              fontSize: AppLayout.getHeight(14),
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(height: AppLayout.getHeight(10)),
                          //-----ICI-----
                          Column(
                            children: [
                              Container(
                                width: AppLayout.getWidth(300),
                                height: AppLayout.getHeight(40),
                                decoration: BoxDecoration(
                                  color: const Color(0xFF029CAD),
                                  // Pas de bord arrondi
                                  borderRadius: BorderRadius.circular(0),
                                  boxShadow: const [
                                    BoxShadow(
                                      color: Colors.black,
                                      offset: Offset(0, 2), // Ombre vers le bas
                                      blurRadius: 4,
                                    ),
                                  ],
                                ),
                                child: Center(
                                  child: TextField(
                                    style: const TextStyle(
                                      // Couleur du texte
                                      color: Colors.white,
                                    ),
                                    decoration: InputDecoration(
                                      hintText: "Entrez votre Email...",
                                      hintStyle: const TextStyle(
                                        color: Colors.white,
                                      ), // Couleur du hint text
                                      border:
                                          InputBorder.none, // Pas de bordure
                                      contentPadding: EdgeInsets.only(
                                        left: AppLayout.getWidth(20),
                                        bottom: AppLayout.getHeight(10),
                                      ), // Padding horizontal pour le hintText
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(height: AppLayout.getHeight(20)),
                              Container(
                                width: AppLayout.getWidth(200),
                                height: AppLayout.getHeight(40),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(
                                      0), // Pas de bord arrondi
                                  boxShadow: const [
                                    BoxShadow(
                                      color: Colors.black,
                                      offset: Offset(0, 2), // Ombre vers le bas
                                      blurRadius: 4,
                                    ),
                                  ],
                                ),
                                child: const Center(
                                  child: Text(
                                    "Commencer gratuitement",
                                    style: TextStyle(color: Color(0xFF029CAD)),
                                  ),
                                ),
                              ),
                            ],
                          )
                          //
                        ],
                      ),
                    ],
                  ),
                ),
                //
                //const FooterSection()
              ],
            ),
          )
        //--Large Screen---
        : SingleChildScrollView(
            child: Container(
              //height: AppLayout.getScreenHeight() * 0.85,
              width: AppLayout.getScreenWidth(),
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(
                      'assets/images/img_3.png'), // Remplacez par le chemin de votre image
                  fit: BoxFit.cover, // Pour remplir tout le container
                ),
              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(width: AppLayout.getWidth(40)),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Unifier vos applications \nen une seule expérience.',
                            style: TextStyle(
                              fontSize: AppLayout.getHeight(45),
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(height: AppLayout.getHeight(30)),
                          Text(
                            'Adipisicing elit, sed do eiusmod tempor incididunt with labore \net dolore magna aliqua enim ad minimum.',
                            style: TextStyle(
                              fontSize: AppLayout.getHeight(14),
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(height: AppLayout.getHeight(20)),
                          //-----ICI-----
                          Row(
                            children: [
                              Container(
                                width: AppLayout.getWidth(300),
                                height: AppLayout.getHeight(40),
                                decoration: BoxDecoration(
                                  color: const Color(0xFF029CAD),
                                  // Pas de bord arrondi
                                  borderRadius: BorderRadius.circular(0),
                                  boxShadow: const [
                                    BoxShadow(
                                      color: Colors.black,
                                      offset: Offset(0, 1), // Ombre vers le bas
                                      blurRadius: 4,
                                    ),
                                  ],
                                ),
                                child: Center(
                                  child: TextField(
                                    style: const TextStyle(
                                      // Couleur du texte
                                      color: Colors.white,
                                    ),
                                    decoration: InputDecoration(
                                      hintText: "Entrez votre Email...",
                                      hintStyle: const TextStyle(
                                        color: Colors.white,
                                      ), // Couleur du hint text
                                      border:
                                          InputBorder.none, // Pas de bordure
                                      contentPadding: EdgeInsets.only(
                                        left: AppLayout.getWidth(20),
                                        bottom: AppLayout.getHeight(10),
                                      ), // Padding horizontal pour le hintText
                                    ),
                                  ),
                                ),
                              ),
                              //SizedBox(width: 10),
                              GestureDetector(
                                onTap: () {
                                  //
                                },
                                child: Container(
                                  width: AppLayout.getWidth(200),
                                  height: AppLayout.getHeight(40),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(
                                        0), // Pas de bord arrondi
                                    boxShadow: const [
                                      BoxShadow(
                                        color: Colors.black,
                                        offset:
                                            Offset(0, 1), // Ombre vers le bas
                                        blurRadius: 4,
                                      ),
                                    ],
                                  ),
                                  child: const Center(
                                    child: Text(
                                      "Commencer gratuitement",
                                      style:
                                          TextStyle(color: Color(0xFF029CAD)),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          )
                          //
                        ],
                      ),
                      SizedBox(
                        height: AppLayout.getScreenHeight() * 0.85,
                        child: const Image(
                          image: AssetImage("assets/images/acceuil.png"),
                        ),
                      ),
                    ],
                  ),
                  // ---footer--
                  //const FooterSection()
                ],
              ),
            ),
          );
  }
}
