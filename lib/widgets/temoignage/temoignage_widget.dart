import 'package:flutter/material.dart';

import '../../constants/app_layout.dart';

class TemoignageWidget extends StatelessWidget {
  final String iconAssetPath; // Chemin de l'image dans les assets
  final String nomPrenom;
  final String titre;
  final String commentaire;

  const TemoignageWidget({
    super.key,
    required this.iconAssetPath,
    required this.nomPrenom,
    required this.titre,
    required this.commentaire,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          SizedBox(
            width: AppLayout.getWidth(50), // Largeur de l'icône
            height: AppLayout.getHeight(50), // Hauteur de l'icône
            child: Image(
              image: AssetImage(iconAssetPath),
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(height: AppLayout.getHeight(20)),
          SizedBox(
            width: AppLayout.getWidth(350), // Largeur de la description
            child: Text(
              commentaire,
              style: TextStyle(
                fontSize: AppLayout.getHeight(14), // Taille de la description
              ),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(height: AppLayout.getHeight(10)),
          Text(
            nomPrenom,
            style: TextStyle(
              fontSize: AppLayout.getHeight(18),
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.center,
          ),
          Text(
            titre,
            style: TextStyle(
              fontSize: AppLayout.getHeight(12),
              color: const Color(0xFF20CDD8),
            ),
            textAlign: TextAlign.center,
          )
        ],
      ),
    );
  }
}
