import 'package:flutter/material.dart';

import '../../constants/app_layout.dart';

class InvestirSection extends StatelessWidget {
  const InvestirSection({super.key});

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return screenSize.width < 1155
        ? Container(
            //
            height: AppLayout.getScreenHeight() * 0.85,
            width: AppLayout.getScreenWidth(),
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage(
                    'assets/images/img_4.png'), // Remplacez par le chemin de votre image
                fit: BoxFit.cover, // Pour remplir tout le container
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                //SizedBox(width: AppLayout.getWidth(40)),
                SizedBox(
                  height: AppLayout.getScreenHeight() * 0.40,
                  child: const Image(
                    image: AssetImage("assets/images/img_1.png"),
                  ),
                ),
                Text(
                  "Nous savons",
                  style: TextStyle(
                    fontSize: AppLayout.getHeight(18),
                    color: Colors.white,
                  ),
                ),
                Text(
                  "Investir de manière \nresponsable financièrement.",
                  style: TextStyle(
                    fontSize: AppLayout.getHeight(25),
                    color: Colors.white,
                  ),
                ),
                //SizedBox(height: AppLayout.getHeight(20)),
                Text(
                  'Adipisicing elit, sed do eiusmod tempor incididunt with labore \net dolore magna aliqua enim ad minimum.',
                  style: TextStyle(
                    fontSize: AppLayout.getHeight(14),
                    color: Colors.white,
                  ),
                ),
                Text(
                  'Adipisicing elit, sed do eiusmod tempor incididunt with labore \net dolore magna aliqua enim ad minimum.',
                  style: TextStyle(
                    fontSize: AppLayout.getHeight(14),
                    color: Colors.white,
                  ),
                ),
                //SizedBox(height: AppLayout.getHeight(20)),
                ElevatedButton(
                  onPressed: () {
                    // Action à effectuer lorsque le bouton est cliqué
                  },
                  style: ElevatedButton.styleFrom(
                    backgroundColor:
                        const Color(0xFFffffff), // Couleur de fond du bouton
                    shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.circular(0), // Bord arrondi à zéro
                    ),
                  ),
                  child: Text(
                    "Apprendre encore plus",
                    style: TextStyle(
                      color: const Color(0xFF029CAD),
                      fontSize: AppLayout.getHeight(16),
                    ),
                  ),
                ),
                SizedBox(height: AppLayout.getHeight(5)),
              ],
            ),
          )
        //----large screen
        : Container(
            //height: AppLayout.getScreenHeight() * 0.85,
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/img_4.png'),
                fit: BoxFit.cover,
              ),
            ),
            child: Container(
              padding: EdgeInsets.only(left: AppLayout.getWidth(300)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: AppLayout.getHeight(35)),
                  Text(
                    "Nous savons",
                    style: TextStyle(
                      fontSize: AppLayout.getHeight(18),
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(height: AppLayout.getHeight(40)),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Investir de manière \nresponsable financièrement.",
                            style: TextStyle(
                              fontSize: AppLayout.getHeight(35),
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(height: AppLayout.getHeight(40)),
                          Text(
                            'Adipisicing elit, sed do eiusmod tempor incididunt with labore \net dolore magna aliqua enim ad minimum.',
                            style: TextStyle(
                              fontSize: AppLayout.getHeight(14),
                              color: Colors.white,
                            ),
                          ),
                          Text(
                            'Adipisicing elit, sed do eiusmod tempor incididunt with labore \net dolore magna aliqua enim ad minimum.',
                            style: TextStyle(
                              fontSize: AppLayout.getHeight(14),
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(height: AppLayout.getHeight(50)),
                          ElevatedButton(
                            onPressed: () {
                              // Action à effectuer lorsque le bouton est cliqué
                            },
                            style: ElevatedButton.styleFrom(
                              backgroundColor: const Color(0xFFffffff),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(0),
                              ),
                            ),
                            child: Text(
                              "Apprendre encore plus",
                              style: TextStyle(
                                color: const Color(0xFF029CAD),
                                fontSize: AppLayout.getHeight(16),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        padding: EdgeInsets.only(
                          bottom: AppLayout.getHeight(120),
                        ),
                        height: AppLayout.getScreenHeight() * 0.85,
                        child: const Image(
                          image: AssetImage("assets/images/img_1.png"),
                        ),
                      ),
                    ],
                  ),
                  //
                ],
              ),
            ),
          );
  }
}
