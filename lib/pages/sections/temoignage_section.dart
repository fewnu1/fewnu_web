import 'package:flutter/material.dart';

import '../../constants/app_layout.dart';
import '../../widgets/temoignage/temoignage_widget.dart';

class TemoignageSection extends StatelessWidget {
  const TemoignageSection({super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(top: AppLayout.getHeight(30)),
            height: AppLayout.getScreenHeight() * 0.85,
            width: AppLayout.getScreenWidth(),
            decoration: const BoxDecoration(
              color: Color(0xFFF6F7FF),
            ),
            child: Column(
              children: [
                Text(
                  "Témoignages",
                  style: TextStyle(
                    fontSize: AppLayout.getHeight(18),
                    color: const Color(0xFF029CAD),
                  ),
                ),
                SizedBox(height: AppLayout.getHeight(20)),
                Text(
                  "Nos clients satisfaits disent",
                  style: TextStyle(
                    fontSize: AppLayout.getHeight(25),
                  ),
                ),
                Text(
                  "À propos de nous",
                  style: TextStyle(
                    fontSize: AppLayout.getHeight(25),
                  ),
                ),
                //------les commentaires-----
                SizedBox(height: AppLayout.getHeight(20)),
                //--ici---
                const TemoignageWidget(
                  iconAssetPath: "assets/images/citation.png",
                  nomPrenom: "Rosalina D. Alian",
                  titre: "Fondateur d'uithemes",
                  commentaire:
                      "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do tempor ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla..",
                ),
              ],
            ),
          ),
          //---footer--
          //const FooterSection()
        ],
      ),
    );
  }
}
