import 'package:flutter/material.dart';

import '../../constants/app_layout.dart';

class ContactWidget extends StatelessWidget {
  const ContactWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black,
      //height: AppLayout.getScreenHeight() * 0.75,
      padding: EdgeInsets.symmetric(
        horizontal: AppLayout.getWidth(60),
        vertical: AppLayout.getWidth(15),
      ),
      child: Column(
        children: [
          // Première ligne : Nom et Email
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'ENTRER VOTRE NOM',
                      style: TextStyle(color: Colors.white),
                    ),
                    SizedBox(height: AppLayout.getHeight(10)),
                    TextFormField(
                      style: const TextStyle(color: Colors.white),
                      decoration: const InputDecoration(
                        hintText: 'Votre nom :',
                        hintStyle: TextStyle(color: Colors.white),
                        suffixIcon: Icon(
                          Icons.person,
                          color: Colors.white,
                        ),
                        filled: true,
                        fillColor: Color(0xFF292929),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(width: AppLayout.getWidth(25)),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'ENTRER VOTRE EMAIL',
                      style: TextStyle(color: Colors.white),
                    ),
                    SizedBox(height: AppLayout.getHeight(10)),
                    TextFormField(
                      style: const TextStyle(color: Colors.white),
                      decoration: const InputDecoration(
                        hintText: 'Email :',
                        hintStyle: TextStyle(color: Colors.white),
                        suffixIcon: Icon(
                          Icons.email,
                          color: Colors.white,
                        ),
                        filled: true,
                        fillColor: Color(0xFF292929),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: AppLayout.getHeight(30)),

          // Deuxième ligne : Téléphone et Sujet
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'ENTRER VOTRE TELEPHONE',
                      style: TextStyle(color: Colors.white),
                    ),
                    SizedBox(height: AppLayout.getHeight(10)),
                    TextFormField(
                      style: const TextStyle(color: Colors.white),
                      decoration: const InputDecoration(
                        hintText: 'Téléphone :',
                        hintStyle: TextStyle(color: Colors.white),
                        suffixIcon: Icon(Icons.phone, color: Colors.white),
                        filled: true,
                        fillColor: Color(0xFF292929),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(width: AppLayout.getWidth(25)),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'ENTRER VOTRE SUJET',
                      style: TextStyle(color: Colors.white),
                    ),
                    SizedBox(height: AppLayout.getHeight(10)),
                    TextFormField(
                      style: const TextStyle(color: Colors.white),
                      decoration: const InputDecoration(
                        hintText: 'Sujet :',
                        hintStyle: TextStyle(color: Colors.white),
                        suffixIcon: Icon(Icons.subject, color: Colors.white),
                        filled: true,
                        fillColor: Color(0xFF292929),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: AppLayout.getHeight(30)),

          // Troisième ligne : Message
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'ENTRER VOTRE MESSAGE',
                style: TextStyle(color: Colors.white),
              ),
              SizedBox(height: AppLayout.getHeight(10)),
              TextFormField(
                style: const TextStyle(color: Colors.white),
                maxLines: 3,
                decoration: const InputDecoration(
                  suffixIcon: Icon(Icons.message, color: Colors.white),
                  filled: true,
                  fillColor: Color(0xFF292929),
                ),
              ),
            ],
          ),
          SizedBox(height: AppLayout.getHeight(25)),

          // Dernière ligne : Bouton rectangulaire
          ElevatedButton(
            onPressed: () {
              // Action à effectuer lorsque le bouton est cliqué
            },
            style: ElevatedButton.styleFrom(
              backgroundColor: const Color(0xFF029CAD),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(0),
              ),
            ),
            child: Text(
              "Envoyé",
              style: TextStyle(
                color: Colors.white,
                fontSize: AppLayout.getHeight(16),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
