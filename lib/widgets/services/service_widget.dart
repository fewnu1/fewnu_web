import 'package:flutter/material.dart';

import '../../constants/app_layout.dart';

class ServiceWidget extends StatelessWidget {
  final String iconAssetPath; // Chemin de l'image dans les assets
  final String title;
  final String description;

  const ServiceWidget({
    super.key,
    required this.iconAssetPath,
    required this.title,
    required this.description,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(AppLayout.getWidth(16)),
      decoration: const BoxDecoration(
        color: Colors.white,
      ),
      child: Column(
        children: [
          Image.asset(
            iconAssetPath,
            width: AppLayout.getWidth(40), // Largeur de l'image
            height: AppLayout.getHeight(40), // Hauteur de l'image
          ),
          SizedBox(height: AppLayout.getHeight(15)),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                title,
                style: TextStyle(
                  fontSize: AppLayout.getHeight(18), // Taille du titre
                  fontWeight: FontWeight.bold, // Style du titre
                ),
              ),
              SizedBox(height: AppLayout.getHeight(10)),
              SizedBox(
                width: AppLayout.getWidth(200), // Largeur de la description
                child: Text(
                  description,
                  style: TextStyle(
                    fontSize: AppLayout.getHeight(14),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: AppLayout.getHeight(10)),
          GestureDetector(
            onTap: () {
              // Action à effectuer lorsque le cercle est cliqué
            },
            child: Container(
              width: AppLayout.getWidth(40), // Largeur du cercle
              height: AppLayout.getHeight(40), // Hauteur du cercle
              decoration: const BoxDecoration(
                color: Color(0xFFF1F1FF), // Couleur du cercle
                shape: BoxShape.circle,
              ),
              child: const Center(
                child: Icon(
                  Icons.arrow_forward,
                  color: Color(
                    0xFFBBBEE0,
                  ), // Couleur de l'icône à l'intérieur du cercle
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
